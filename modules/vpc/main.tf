provider "aws" {
  region = "eu-west-2"
}
terraform {
  		# The configuration for this backend will be filled in by Terragrunt
	  	backend "s3" {}
		}

data "aws_availability_zones" "available" {}

/*
 * Create the New environment (peer)vpc
 */
resource "aws_vpc" "vpc" {
  cidr_block           = "${var.cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    Name        = "${var.name}"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Public 1
resource "aws_subnet" "publicsubnet1" {
  cidr_block        = "${var.public1}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name        = "${var.environment}-public1"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Public 2
resource "aws_subnet" "publicsubnet2" {
  cidr_block        = "${var.public2}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name        = "${var.environment}-public2"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Private 1
resource "aws_subnet" "privatesubnet1" {
  cidr_block        = "${var.private1}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"

  tags {
    Name        = "${var.environment}-private1"
    environment = "${var.environment}"
  }
}

# Create VPC Subnet - Private 2
resource "aws_subnet" "privatesubnet2" {
  cidr_block        = "${var.private2}"
  vpc_id            = "${aws_vpc.vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"

  tags {
    Name        = "${var.environment}-private2"
    environment = "${var.environment}"
  }
}

# Internet access for the new Environment VPC. Creates Internet gateway, NAT Gateway and Elastic IP.
resource "aws_eip" "env_nateip" {
  vpc = true
}

resource "aws_internet_gateway" "env_gw" {
  vpc_id = "${aws_vpc.vpc.id}"

tags {
    Name        = "${var.gatewayname}"
    environment = "${var.environment}"
  }
}

resource "aws_nat_gateway" "env_nat" {
  allocation_id = "${aws_eip.env_nateip.id}"
  subnet_id     = "${aws_subnet.publicsubnet1.id}"

  tags {
    Name = "${var.environment}-natgw"
    environment = "${var.environment}"
  }
}

/*
 * Create the PEERLINK.
 */

data "aws_vpc" "core_vpc" {
    filter {
    name   = "tag:Name"
    values = ["mgmt-vpc"]
  }
}
resource "aws_vpc_peering_connection" "peerlink" {
  vpc_id        = "${aws_vpc.vpc.id}"         
  auto_accept   = "${var.auto_accept}"
  peer_vpc_id   = "${data.aws_vpc.core_vpc.id}"             # Peer in this instance is for the peer link, so the remote VPC, which is the Management / CORE VPC.
}


/*
 * Create Routing Tables
 */
# Create the route table object to route between the CORE VPC and the new environment VPC
resource "aws_route_table" "routetocore" {
  vpc_id        = "${aws_vpc.vpc.id}"
    tags {
        Name        = "${var.core_cidr}"
        environment = "${var.environment}"
        }
}
# Route to the Core/Management VPC
resource "aws_route" "coreroute" {
  route_table_id            = "${aws_route_table.routetocore.id}"
  destination_cidr_block    = "${var.core_cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peerlink.id}"
  depends_on                = ["aws_route_table.routetocore"]
}

# Route Table association for new (peer) VPC to the core management VPC. This adds the route table associations to the new Subnets.
resource "aws_route_table_association" "private1tocore" {       # Route for the private 1 subnet to the Core.
    route_table_id  = "${aws_route_table.routetocore.id}"
    subnet_id       = "${aws_subnet.privatesubnet1.id}"
}
resource "aws_route_table_association" "private2tocore" {       # Route for the private 2 subnet to the Core.
    route_table_id  = "${aws_route_table.routetocore.id}"
    subnet_id       = "${aws_subnet.privatesubnet2.id}"
}
resource "aws_route_table_association" "public1tocore" {        # Route for the public 1 subnet to the Core.
    route_table_id  = "${aws_route_table.routetocore.id}"
    subnet_id       = "${aws_subnet.publicsubnet1.id}"
}
resource "aws_route_table_association" "public2tocore" {        # Route for the public 2 subnet to the Core.
    route_table_id  = "${aws_route_table.routetocore.id}"
    subnet_id       = "${aws_subnet.publicsubnet2.id}"
}


# Defining the routes from the CORE VPC to the newly deployed VPC

data "aws_route_table" "core_private" {
  filter {
    name   = "tag:Name"
    values = ["private"]
  }

  filter {
    name    = "vpc-id"
    values  = ["${data.aws_vpc.core_vpc.id}"]
  }
}

data "aws_route_table" "core_public" {
  filter {
    name   = "tag:Name"
    values = ["public"]
  }

  filter {
    name    = "vpc-id"
    values  = ["${data.aws_vpc.core_vpc.id}"]
  }
}

# Create the route from CORE Private subnets to the new VPC
resource "aws_route" "privatecoretonewcidr" {
  route_table_id            = "${data.aws_route_table.core_private.id}"
  destination_cidr_block    = "${var.cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peerlink.id}"
  depends_on                = ["aws_vpc_peering_connection.peerlink"]
}

# Create the route from CORE Public subnets to the new VPC
resource "aws_route" "publiccoretonewcidr" {
  route_table_id            = "${data.aws_route_table.core_public.id}"
  destination_cidr_block    = "${var.cidr}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.peerlink.id}"
  depends_on                = ["aws_vpc_peering_connection.peerlink"]
}



# OUTPUTS

output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "privatesubnet1_id" {
  value = "${aws_subnet.privatesubnet1.id}"
}

output "privatesubnet2_id" {
  value = "${aws_subnet.privatesubnet2.id}"
}

output "publicsubnet1_id" {
  value = "${aws_subnet.publicsubnet1.id}"
}

output "publicsubnet2_id" {
  value = "${aws_subnet.publicsubnet2.id}"
}

output "env_natgw_id" {
  value = "${aws_nat_gateway.env_nat.id}"
}

output "peerlink_id" {
    value = "${aws_vpc_peering_connection.peerlink.id}"
}

output "route_table_id" {
    value = "${aws_route_table.routetocore.id}"
}


