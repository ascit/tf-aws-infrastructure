variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
  default     = "eu-west-2"
}

variable "key_name" {
  description = "ssh keypair file downloaded from AWS"
}

variable "cidr" {
  description = "the CIDR block to provision for the VPC, if set to something other than the default, both internal_subnets and external_subnets have to be defined as well"
}

variable "public1" {
  description = "CIDR for the 1st Public facing subnet"
}

variable "public2" {
  description = "CIDR for the 2nd Public facing subnet"
}

variable "private1" {
  description = "CIDR for the 1st Private subnet"
}

variable "private2" {
  description = "CIDR for the 2nd Private subnet"
}

variable "environment" {
  description = "Environment tag, e.g prod"
}



variable "name" {
  description = "Name tag for the VPC"
}

variable "gatewayname" {
  description = "Name tag, e.g mgmt-gateway"
}

# Variables for the CORE (Management) VPC which already exists.
variable "core_cidr" {
    description = "CIDR range of the management VPC"
}
variable "private1_core" {
    description = "ID of the 1st Private subnet in the management VPC"
}
variable "private2_core" {
    description = "ID of the 2nd Private subnet in the management VPC"
}
variable "public1_core" {
    description = "ID of the 1st Public subnet in the management VPC"
}
variable "public2_core" {
    description = "ID of the 2nd public subnet in the management VPC"
}
variable "auto_accept" {
    default     = "true"
}