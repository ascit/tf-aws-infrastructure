terragrunt = {
  remote_state {
    backend = "s3"
    config {
      encrypt        = true
      bucket         = "gps-aws-tfstate"
      key            = "${path_relative_to_include()}/terraform.tfstate"
      region         = "eu-west-2"
      dynamodb_table = "devgps-tf-locks"
    }
  }
}