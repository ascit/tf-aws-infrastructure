/* 
INPUT VARIABLES 
    key_name
    db_instance_type
    db_count
    db_subnet
    db_security_group
    environment
*/


/*
 * This block finds the private GPS ami for SQL Server deployments (Windows Server 2008R2 with SQL Server installed using MSDN SQL license).
    Other filters should probably be added to this block. Maybe environment or SQL version.
 */
data "aws_ami" "db_ami" {
  most_recent = true                # Where multiple AMIs are found that match the other filters, the most recent will be used.
  owners = [""]         # will restrict the search to only AMIs owned by this account (GPS-Dev)
  filter {
    name   = "tag:role"             # Filters the retunred AMIs using the assigned tags. In this case the tag name is 'role'. All AMIs should have a role tag defined.
    values = ["sql server"]         # This is the value of the 'role' tag being searched for.
  }
  filter { 
    name    = "tag:OS_Version"      # An additional filter. In this case another tag named 'OS_Version'. All AMIs should have this tag defined.
    values  = ["Windows"]           # Only AMis with 'Windows' as the OS_Version tag will be returned.
  }
}

### INLINE - Bootsrap Windows Server 2012 R2 ###
data "template_file" "init" {
   template = <<EOF
   <powershell>
   Set-psRepository -Name PSGallery -InstallationPolicy Trusted
   install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
   Install-Module -Name xWebDeploy
   Install-Module -Name xSqlPs
   Install-Module -Name xDSCDomainjoin
   Install-Module -Name xInstallDotNet462Framework
   Install-Module -Name xDSCFirewall
   </powershell>
EOF
}

# key pair for all server deployments
variable "key_name" {
  default = ""
}
# db instance details
variable "db_instance_type" {
    description = "the size of instance to be deployed. For example T2.medium"
    default = "t2.small"
}
variable "db_count" {
  description = "The number of db servers to be deployed"
  default = "1"
}
variable "db_subnet" {
    description = "Subnet ID where the new DB server will be deployed. Obtain the ID from the VPC"
}
variable "db_security_group" {
    description = "Security groups to be assigned to the new instance. Use security Groups IDs from the VPC where the new instance is to be deployed."
    type = "list"
}

# other variables
variable "environment" {
    description = "name of the environment where the instance will be deployed. For example 'QA' or 'dev'"

}

# Resources to be deployed
resource "aws_instance" "db_server" {
  ami                     = "${data.aws_ami.db_ami.id}"
  vpc_security_group_ids  = ["${var.db_security_group}"]
  instance_type           = "${var.db_instance_type}"
  subnet_id               = "${var.db_subnet}"
  key_name                = "${var.key_name}"
  count                   = "${var.db_count}"
 # user_data               = "${data.template_file.init.rendered}"
  
  tags {
    Name          = "${var.environment}_db_${format("%03d", count.index + 1)}"
    environment   = "${var.environment}"
    role          = "db server"
  }
}