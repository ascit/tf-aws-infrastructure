output "web_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = ["${aws_instance.web_server.private_ip}"]
}

output "db_ip" {
  description = "List of private IP addresses assigned to the instances"
  value       = ["${aws_instance.db_server.private_ip}"]
}



output "app_id" {
  description = "List of IDs of instances"
  value       = ["${aws_instance.app_server.id}"]
}

output "web_id" {
  description = "List of IDs of instances"
  value       = ["${aws_instance.web_server.id}"]
}


output "db_id" {
  description = "List of IDs of instances"
  value       = ["${aws_instance.db_server.id}"]
}


