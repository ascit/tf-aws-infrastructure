# AMI for Web server deployments
data "aws_ami" "web_ami" {
  most_recent = true
  owners = ["477661052598"]
  filter {
    name   = "tag:role"
    values = ["web server"]
  }
  filter { 
    name    = "tag:OS_Version"
    values  = ["Windows"]
  }
}

# AMI for App server deployments
data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["477661052598"]
  filter {
    name   = "tag:role"
    values = ["app server"]
  }
  filter { 
    name    = "tag:OS_Version"
    values  = ["Windows"]
  }
}


### INLINE - Bootstrap Windows Server 2012 R2 ###
data "template_file" "init" {
   template = <<EOF
   <powershell>
   Set-psRepository -Name PSGallery -InstallationPolicy Trusted
   install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
   Install-Module -Name xWebDeploy
   Install-Module -Name xSqlPs
   Install-Module -Name xDSCDomainjoin
   Install-Module -Name xInstallDotNet462Framework
   Install-Module -Name xDSCFirewall
   </powershell>
EOF
}

data "template_file" "init_app" {
   template = <<EOF
   <powershell>
   Set-psRepository -Name PSGallery -InstallationPolicy Trusted
   install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
   Install-Module -Name xWebDeploy
   Install-Module -Name xSqlPs
   Install-Module -Name xDSCDomainjoin
   Install-Module -Name xInstallDotNet462Framework
   Install-Module -Name xDSCFirewall
   </powershell>
EOF
}


/*
 * This is the block of code to use when the SQL image has been created using MSDN copy of SQL.
 # AMI for SQL Server deployments
 */
data "aws_ami" "db_ami" {
  most_recent = true
  owners = ["477661052598"]
  filter {
    name   = "tag:role"
    values = ["sql server"]
  }
  filter { 
    name    = "tag:OS_Version"
    values  = ["Windows"]
  }
}


# key pair for all server deployments
variable "key_name" {
}

# web server details
variable "web_instance_type" {
}
variable "web_count" {
  description = "The number of web servers to be deployed"
  default = "1"
}
variable "web_subnet" {
}

variable "web_security_group" {
  type = "list"
}

# App server details
variable "app_instance_type" {
}
variable "app_count" {
  description = "The number of app servers to be deployed"
  default = "1"
}
variable "app_subnet" {

}
variable "app_security_group" {
  type = "list"
  
}

# Db server details
variable "db_instance_type" {
}
variable "db_count" {
  description = "The number of db servers to be deployed"
  default = "1"
}
variable "db_subnet" {

}
variable "db_security_group" {
  type = "list"
  
}


# other variables
variable "environment" {

}


/*
 * Deployment of EC2 instances
 *
*/

#

resource "aws_instance" "web_server" {
  ami                     = "${data.aws_ami.web_ami.id}"
  vpc_security_group_ids  = ["${var.web_security_group}"]
  instance_type           = "${var.web_instance_type}"
  subnet_id               = "${var.web_subnet}"
  key_name                = "${var.key_name}"
  count                   = "${var.web_count}"
/*  user_data              = <<EOF
   <powershell>
   Set-psRepository -Name PSGallery -InstallationPolicy Trusted
   install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
   Install-Module -Name xWebDeploy
   Install-Module -Name xSqlPs
   Install-Module -Name xDSCDomainjoin
   Install-Module -Name xInstallDotNet462Framework
   Install-Module -Name xDSCFirewall
   </powershell>
   EOF
*/
  
  tags {
    Name          = "${var.environment}_web_${format("%03d", count.index + 1)}"
    environment   = "${var.environment}"
    role          = "web server"
  }
}

# 
 #  <script>
 #    winrm quickconfig -q & winrm set winrm/config/winrs @{MaxMemoryPerShellMB="300"} & winrm set winrm/config @{MaxTimeoutms="1800000"} & winrm set winrm/config/service @{AllowUnencrypted="true"} & winrm set winrm/config/service/auth @{Basic="true"} & winrm/config @{MaxEnvelopeSizekb="8000kb"}
 #  </script>
resource "aws_instance" "app_server" {
  ami                     = "${data.aws_ami.app_ami.id}"
  vpc_security_group_ids  = ["${var.app_security_group}"]
  instance_type           = "${var.app_instance_type}"
  subnet_id               = "${var.app_subnet}"
  key_name                = "${var.key_name}"
  count                   = "${var.app_count}"
  
  tags {
    Name          = "${var.environment}_app_${format("%03d", count.index + 1)}"
    environment   = "${var.environment}"
    role          = "app server"
  }
}

# 
resource "aws_instance" "db_server" {
  ami                     = "${data.aws_ami.db_ami.id}"
  vpc_security_group_ids  = ["${var.db_security_group}"]
  instance_type           = "${var.db_instance_type}"
  subnet_id               = "${var.db_subnet}"
  key_name                = "${var.key_name}"
  count                   = "${var.db_count}"
 # user_data               = "${data.template_file.init.rendered}"
  
  tags {
    Name          = "${var.environment}_db_${format("%03d", count.index + 1)}"
    environment   = "${var.environment}"
    role          = "db server"
  }
}

